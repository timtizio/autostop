'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:MessagerieController
 * @description
 * # MessagerieController
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('MessagerieController', [
    '$scope',
    function ( $scope ) {
        $scope.journey = {
            start: {
                city: 'Marseille'
            },
            end: {
                city: 'Le Cheylard'
            }
        };

        $scope.messages = [
            {
                id: 2,
                subject: 'Annonce pour le trajet Saint Sauveur -> Privas',
                sender: {
                    name: 'Jean-Bernard Huet'
                },
                notRead: 0
            },
            {
                id: 0,
                subject: 'Annonce pour le trajet Le Cheylard -> Privas',
                sender: {
                    name: 'Manon Gauthier'
                },
                notRead: 1
            },
        ];
    }
]);
