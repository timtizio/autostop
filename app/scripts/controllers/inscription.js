'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:InscriptionController
 * @description
 * # InscriptionController
 * Controller of the autostopApp
 */
angular.module('autostopApp')
  .controller('InscriptionController', [ '$scope', function ( $scope ) {
    var initUser = function() {
        $scope.user = {
            'surname': '',
            'name': '',
            'email': '',
            'password': {
                'first': '',
                'second': ''
            }
        };
    };

    $scope.onClickValider = function() {
        console.log( $scope.user );
        initUser();
    };


  }]);
