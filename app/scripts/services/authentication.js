'use strict';

angular.module( 'autostopApp' )
.service( 'AuthService', [
    '$cookies',
    function( $cookies ) {

        this.users = [
            {
                'id': 1,
                'email': 'timothe@simplon-ve-2017.fr',
                'password': 'timothe',
                'userName': 'Timothé'
            },
            {
                'id': 2,
                'email': 'admin',
                'password': 'admin',
                'userName': 'Administrateur'
            }
        ];

        this.login = function( email, password ) {
            var retour = false;

            this.users.forEach( function( user ) {
                if( email === user.email && password === user.password ) {
                    $cookies.put( 'userId', user.id );
                    $cookies.put( 'userName', user.userName );
                    retour = true;
                }
            });
            return retour;
        };

        this.logout = function() {
            $cookies.remove( 'userId' );
            $cookies.remove( 'userName' );
            console.log( $cookies.getAll() );
        };

        this.isLogged = function() {
            var retour = false;
            if ( $cookies.get( 'userId' ) && $cookies.get( 'userName' ) ) {
                retour = {
                    'id': $cookies.get( 'userId' ),
                    'name': $cookies.get( 'userName' )
                };
            }
            return retour;
        };
    }
]);