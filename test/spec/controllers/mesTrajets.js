'use strict';

describe('Controller: MestrajetsCtrl', function () {

  // load the controller's module
  beforeEach(module('autostopApp'));

  var MestrajetsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MestrajetsCtrl = $controller('MestrajetsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  /* it('should attach a list of awesomeThings to the scope', function () {
    expect(MestrajetsCtrl.awesomeThings.length).toBe(3);
  }); */
});
