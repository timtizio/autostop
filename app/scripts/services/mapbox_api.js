'use strict';

angular.module( 'autostopApp' )
.service('MapboxAPIService', [
    '$http',
    '$q',
    function( $http, $q ) {
        var mapboxToken = 'pk.eyJ1IjoidGltYWp0IiwiYSI6ImNqYWsxdDFwNDN4MnUzM251Z282dnRyOXMifQ.gprz6u6FDQkrZaWLIG0A8g';
        var MAX_SUGGESTION = 5;

        this.search = function( query ) {

            var locations = this.getEmptyLocations();

            var deferred = $q.defer();
            
            if( query === '' ) {
                deferred.resolve(locations);
                return deferred.promise;
            }
            
            // On remplace les espaces par %20 pour l'URL
            query = query.replace(/\s/g,'%20');
            
            this.makeQuery( '/geocoding/v5/mapbox.places/' + query + '.json', 'country=FR&' )
            .then( function( response ) {
                if( response.status === 200 ) {
                    console.log( response );
                    
                    response.data.features.forEach( function ( item ) {
                        locations.places.push({
                            name: item.place_name,
                            coordinate: item.geometry.coordinates,
                            selected: ''
                        });
                    });
                    
                    // On met la premiere suggestion en sected
                    locations.places[0].selected = 'selected';
                    
                    deferred.resolve( locations );
                }
                else {
                    deferred.reject('Aucun lieux trouvé (status de la réponse : ' + response.status + ')');
                }
            });
            
            return deferred.promise;
        };
        
        this.getEmptyLocations = function() {
            return {
                places: [],
                selected: 0,
                up: function() {
                    var oldSelection = this.selected;

                    if (oldSelection === 0) {
                        // On est tout en bas
                        this.selected = MAX_SUGGESTION - 1;
                    }
                    else {
                        this.selected--;
                    }
                    this.refreshSelection();
                },
                down: function() {
                    var oldSelection = this.selected;

                    if ( oldSelection === ( MAX_SUGGESTION - 1 ) ) {
                        // On est tout en haut
                        this.selected = 0;
                    }
                    else {
                        this.selected++;
                    }
                    this.refreshSelection();
                },
                refreshSelection: function() {
                    var that = this;

                    that.places.forEach( function( item, index ) {
                        if( index === that.selected ) {
                            item.selected = 'selected';
                        }
                        else {
                            item.selected = '';
                        }
                    });
                }
            };
        };
        
        this.getDirection = function( coordinates ) {
            var deferred = $q.defer();
            
            this.makeQuery( '/directions/v5/mapbox/driving/' + coordinates[0][0] + ',' + coordinates[0][1] + ';' + coordinates[1][0] + ',' + coordinates[1][1], 'geometries=geojson&' )
            .then( function( response ) {
                
                if( response.status === 200 ) {
                    deferred.resolve( response.data );
                }
                else {
                    deferred.reject('Itinéraire non trouvé (status de la réponse : ' + response.status + ')');
                }
                
            });
            
            return deferred.promise;
        };
        
        ////
        this.makeQuery = function( query, options ) {

            return $http.get( 'https://api.mapbox.com' + query + '?' + options + 'access_token=' + mapboxToken );
        };
    }
]);