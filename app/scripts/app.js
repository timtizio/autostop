'use strict';

/**
 * @ngdoc overview
 * @name autostopApp
 * @description
 * # autostopApp
 *
 * Main module of the application.
 */
angular
  .module('autostopApp', [
    'ngRoute',
    'ngAnimate',
    'ngCookies',
    'ui.bootstrap',
    'ngSanitize'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/rechercher-trajet', {
        templateUrl: 'views/postJourney.html',
        controller: 'PostJourneyController',
        controllerAs: 'about'
      })
      .when('/proposer-trajet', {
        templateUrl: 'views/postJourney.html',
        controller: 'PostJourneyController',
        controllerAs: 'about'
      })
      .when('/inscription', {
        templateUrl: 'views/inscription.html',
        controller: 'InscriptionController',
        controllerAs: 'inscription'
      })
      .when('/resultats/:idJourney', {
        templateUrl: 'views/resultats.html',
        controller: 'ResultatsController',
        controllerAs: 'resultats'
      })
      .when('/messagerie', {
        templateUrl: 'views/messagerie.html',
        controller: 'MessagerieController',
        controllerAs: 'messagerie'
      })
      .when('/messagerie/discussion/:idDiscussion', {
        templateUrl: 'views/discussion.html',
        controller: 'DiscussionController',
        controllerAs: 'discussion'
      })
      .when('/contact/:idAnnonce', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/mes-trajets', {
        templateUrl: 'views/mesTrajets.html',
        controller: 'MestrajetsController',
        controllerAs: 'mesTrajets'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
