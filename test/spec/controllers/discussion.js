'use strict';

describe('Controller: DiscussionCtrl', function () {

  // load the controller's module
  beforeEach(module('autostopApp'));

  var DiscussionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DiscussionCtrl = $controller('DiscussionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  /* it('should attach a list of awesomeThings to the scope', function () {
    expect(DiscussionCtrl.awesomeThings.length).toBe(3);
  }); */
});
