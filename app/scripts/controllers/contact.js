'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('ContactCtrl', [ 
    '$scope',
    '$routeParams',
    function ( $scope, $routeParams ) {
        $scope.items = [
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Manon Gauthier'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'Le Village, 07160 Saint-Jean-Roure',
                        'coordinates': [
                            4.428030,
                            44.940539
                        ]
                    },
                    'date': 'Wed Nov 07 2018 12:51:07 GMT+0000 (UTC)',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'Fri Apr 06 2018 13:58:00 GMT+0000 (UTC)',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Timéo Brun'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Chèze, 07160 Le Cheylard',
                        'coordinates': [
                            4.430715,
                            44.906158
                        ]
                    },
                    'date': 'Wed Nov 07 2018 13:00:07 GMT+0000 (UTC)',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'Fri Apr 06 2018 14:08:00 GMT+0000 (UTC)',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Jean-Bernard Huet'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Brugière, 07190 Saint-Sauveur-de-Montagut',
                        'coordinates': [
                            4.582509,
                            44.820274
                        ]
                    },
                    'date': 'Wed Nov 07 2018 12:51:07 GMT+0000 (UTC)',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'Fri Apr 06 2018 08:48:00 GMT+0000 (UTC)',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
        ];


        $scope.index = $routeParams.idAnnonce;

        $scope.journey = {
            nbPlaces: 3,
            start: {
                location: {
                    name: 'Le Cheylard, 07160',
                    coordinates: [4.491286, 44.884790],
                    city: 'Le Cheylard'
                }
            },
            end: {
                location: {
                    name: 'Privas, 07000',
                    coordinates: [ 4.599039,44.735269],
                    city: 'Privas'
                }
            }
        };
    }
]);
