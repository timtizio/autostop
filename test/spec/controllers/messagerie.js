'use strict';

describe('Controller: MessagerieController', function () {

  // load the controller's module
  beforeEach(module('autostopApp'));

  var MessagerieCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MessagerieCtrl = $controller('MessagerieController', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  /* it('should attach a list of awesomeThings to the scope', function () {
    expect(MessagerieCtrl.awesomeThings.length).toBe(3);
  }); */
});
