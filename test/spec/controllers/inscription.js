'use strict';

describe('Controller: InscriptionController', function () {

  // load the controller's module
  beforeEach(module('autostopApp'));

  var InscriptionController,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InscriptionController = $controller('InscriptionController', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

//   it('should attach a list of awesomeThings to the scope', function () {
//     expect(scope.selected.length).toBe(3);
//   });
});
