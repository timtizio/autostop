'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the autostopApp
 */
angular
    .module('autostopApp')
    .controller('MainController', [ 
        '$scope',
        '$routeParams',
        function ( $scope, $routeParams ) {

            $scope.errorNotRegister = false;
            if( $routeParams.redirect === 'not-register' ) {
                $scope.errorNotRegister = true;
            }

    
            $scope.searchJourney = function() {

            };

            $scope.offerJourney = function() {

            };
    }
]);
