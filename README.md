# autostop


#### Master
[![GitLabl Build Status](https://gitlab.com/timdu13/autostop/badges/master/build.svg)](https://gitlab.com/timdu13/autostop/commits/master)


#### Development
[![GitLabl Build Status](https://gitlab.com/timdu13/autostop/badges/development/build.svg)](https://gitlab.com/timdu13/autostop/commits/development)


## Description

Cette application est réalisé dans le cadre de ma formation [Simplon-VE](https://simplon-ve.fr/).  

Le projet consiste en une plateforme réunissant des utilisateurs désirant se déplacer sur le
territoire ardéchois. Ils remplissent un formulaire pour donner des informations sur leurs
trajets (réguliers ou non), et définissent s’ils recherchent un véhicule, ou s’ils proposent de
prendre un ou plusieurs passagers avec eux.  

Cette plateforme permet de mettre en relation ces utilisateurs en temps réel.

## Installer

Cloner le répertoire git :  
`$ git clone https://gitlab.com/timdu13/autostop/`  

Télécharger les dépendances :  
`$ npm i`  
`$ bower install`  

## Développer

Lancer le serveur de développement :  
`$ grunt serve`  

## Tester

Pour lancer les test du projet, taper la commande `$ grunt test`.  
Le framework de test unitaire utilisé est **Karma**.

## Compiler

Pour compiler le projet lancer la commande `$ grunt`.



*This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.*