'use strict';

describe('Controller: MainController', function () {

  // load the controller's module
  beforeEach(module('autostopApp'));

  var MainController,
    scope,
    routeParams;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $routeParams) {
    scope = $rootScope.$new();
    routeParams = $routeParams;
    MainController = $controller('MainController', {
      $scope: scope,
      $routeParams: routeParams
      // place here mocked dependencies
    });
  }));


});
